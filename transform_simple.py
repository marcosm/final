import sys
from images import read_img, write_img
from transforms import rotate_right, mirror, blur, grayscale

def apply_transformation(file_name, transformation_func):
    pixels = read_img(file_name)

    if transformation_func == 'rotate_right':
        transformed_pixels = rotate_right(pixels)
        output_file = f"{file_name.split('.')[0]}_trans_rotated.png"
    elif transformation_func == 'mirror':
        transformed_pixels = mirror(pixels)
        output_file = f"{file_name.split('.')[0]}_trans_mirrored.png"

    elif transformation_func == 'blur':
        transformed_pixels = blur(pixels)
        output_file = f"{file_name.split('.')[0]}_trans_blurred.png"

    elif transformation_func == 'greyscale':
        transformed_pixels = grayscale(pixels)
        output_file = f"{file_name.split('.')[0]}_trans_grayscale.png"

    else:
        print("Función de transformación no válida.")
        return

    write_img(transformed_pixels, output_file)
    print(f"Imagen transformada guardada como {output_file}")

def main():
    if len(sys.argv) != 3:
        print("Uso: python transform_simple.py <nombre_imagen> <rotate_left|mirror>")
        return

    file_name = sys.argv[1]
    transformation_func = sys.argv[2]

    apply_transformation(file_name, transformation_func)

if __name__ == '__main__':
    main()
