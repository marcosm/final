import images
def change_colors(image: list[list[tuple[int, int, int]]],
                 to_change: tuple[int, int, int],
                 to_change_to: tuple[int, int, int]) -> list[list[tuple[int, int, int]]]:
    new_imagen = []
    for row in image:
        new_fila = []
        for pixel in row:
            if pixel == to_change:
                new_fila.append(to_change_to)
            else:
                new_fila.append(pixel)
        new_imagen.append(new_fila)
    return new_imagen

def rotate_right(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
    ancho = len(image)
    altura = len(image[0])
    imagen_rot = [[(0, 0, 0)] * ancho for _ in range(altura)]

    for i in range(altura):
        for j in range(altura):
            imagen_rot[j][altura - i - 1] = image[i][j]

    return imagen_rot

def mirror(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
    ancho = len(image)
    altura = len(image[0])
    imagen_mirror = [[(0, 0, 0) for _ in range(altura)] for _ in range(ancho)]

    for i in range(ancho):
        for j in range(altura):
            imagen_mirror[i][j] = image[ancho - 1 - i][j]
    return imagen_mirror

def rotate_colors(image: list[list[tuple[int, int, int]]], increment: int) -> list[list[tuple[int, int, int]]]:
    imagen_rotcolor = []

    for row in image:
        new_fila = []
        for pixel in row:
            adjusted_increment = increment
            if increment < 0:
                adjusted_increment = 256 + increment

            new_pixel = tuple((component + adjusted_increment) % 256 for component in pixel)
            new_fila.append(new_pixel)
        imagen_rotcolor.append(new_fila)

    return imagen_rotcolor

def blur(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
    ancho = len(image)
    altura = len(image[0])

    imagen_blur = [[(0, 0, 0) for _ in range(altura)] for _ in range(ancho)]

    for i in range(ancho):
        for j in range(altura):
            total_pixels = 0
            total_red = 0
            total_green = 0
            total_blue = 0

            for x, y in [(i - 1, j), (i + 1, j), (i, j - 1), (i, j + 1), (i - 1, j - 1), (i - 1, j + 1), (i + 1, j - 1),
                         (i + 1, j + 1)]:

                if 0 <= x < ancho and 0 <= y < altura:
                    total_pixels += 1
                    pixel = image[x][y]
                    total_red += pixel[0]
                    total_green += pixel[1]
                    total_blue += pixel[2]

            if total_pixels > 0:
                averaged_red = total_red // total_pixels
                averaged_green = total_green // total_pixels
                averaged_blue = total_blue // total_pixels
                imagen_blur[i][j] = (averaged_red, averaged_green, averaged_blue)
    return imagen_blur

def shift(image: list[list[tuple[int, int, int]]], horizontal: int, vertical: int) ->list[list[tuple[int, int, int]]]:

    ancho = len(image)
    altura = len(image[0])

    imagen_shift = [[(0, 0, 0) for _ in range(altura)] for _ in range(ancho)]

    for y in range(ancho):
        for x in range(altura):
            new_x = x + horizontal
            new_y = y + vertical

            if 0 <= new_x < altura and 0 <= new_y < ancho:
                imagen_shift[new_y][new_x] = image[y][x]

    return imagen_shift

def crop(image: list[list[tuple[int, int, int]]], x: int, y: int, width: int, height: int) ->list[list[tuple[int, int, int]]]:

    imagen_crop = []

    for i in range(y, y + height):
        crop_fila = []
        for j in range(x, x + width):
            crop_fila.append(image[i][j])
        imagen_crop.append(crop_fila)

    return imagen_crop

def grayscale(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
    imagen_gray = []
    for row in image:
        new_fila = []
        for pixel in row:
            gray_value = int((pixel[0] + pixel[1] + pixel[2]) / 3)
            new_pixel = (gray_value, gray_value, gray_value)
            new_fila.append(new_pixel)
        imagen_gray.append(new_fila)
    return imagen_gray


def filter(image: list[list[tuple[int, int, int]]], r: float, g: float, b: float) -> list[list[tuple[int, int, int]]]:

    imagen_filter = []
    for row in image:
        fila_filter = []
        for pixel in row:
            red = min(int(pixel[0] * r), 255)
            green = min(int(pixel[1] * g), 255)
            blue = min(int(pixel[2] * b), 255)
            fila_filter.append((red, green, blue))
        imagen_filter.append(fila_filter)
    return imagen_filter
