import sys
from images import read_img, write_img
from transforms import change_colors, rotate_colors, shift, crop, mirror, blur, grayscale, filter, rotate_right
def apply_transforms(file_name, transformations):
    pixels = read_img(file_name)

    i = 0
    while i < len(transformations):
        transform_function = transformations[i].lower()
        args = transformations[i + 1: i + 4]

        if transform_function == 'change_colors':
            if len(args) != 6:
                print("La función change_colors necesita 6 valores RGB para el cambio.")
                return
            color_to_change = tuple(args[:3])
            new_color = tuple(args[3:])
            pixels = change_colors(pixels, color_to_change, new_color)
            output_file = f"{file_name.split('.')[0]}_2x_color_change.png"
            write_img(pixels, output_file)
            print(f"Imagen con colores cambiados guardada como {output_file}")

        elif transform_function == 'rotate_colors':
            if len(args) != 1:
                print("La función rotate_colors necesita un valor de incremento.")
                return
            increment = int(args[0])

            changed_pixels = rotate_colors(pixels, increment)
            i += 1

            output_file = f"{file_name.split('.')[0]}_2x_rotated_colors.png"
            write_img(changed_pixels, output_file)
            print(f"Imagen con colores rotados guardada como {output_file}")

        elif transform_function == 'shift':
            if len(args) != 2:
                print("La función shift requiere dos números enteros: horizontal y vertical.")
                return

            horizontal_shift = int(args[0])
            vertical_shift = int(args[1])

            shifted_pixels = shift(pixels, horizontal_shift, vertical_shift)

            output_file = f"{file_name.split('.')[0]}_2x_shifted.png"
            write_img(shifted_pixels, output_file)
            print(f"Imagen desplazada guardada como {output_file}")
        elif transform_function == 'crop':
            if len(args) != 4:
                print("La función crop requiere cuatro números enteros: x, y, ancho, altura.")
                return

            x = int(args[0])
            y = int(args[1])
            width = int(args[2])
            height = int(args[3])

            cropped_pixels = crop(pixels, x, y, width, height)

            output_file = f"{file_name.split('.')[0]}_2x_cropped.png"
            write_img(cropped_pixels, output_file)
            print(f"Imagen recortada guardada como {output_file}")

        elif transform_function == 'filtro':
            if len(args) != 3:
                print("La función filter necesita 3 valores para los multiplicadores r, g, y b.")
                return

            filtered_pixels = filter(pixels, *args)

            output_file = f"{file_name.split('.')[0]}_2x_filtered.png"
            write_img(filtered_pixels, output_file)
            print(f"Imagen con filtro guardada como {output_file}")

        elif transform_function == 'mirror':
            transformed_pixels = mirror(pixels)
            output_file = f"{file_name.split('.')[0]}_2x_trans_mirrored.png"

        elif transform_function == 'blur':
            transformed_pixels = blur(pixels)
            output_file = f"{file_name.split('.')[0]}_2x_trans_blurred.png"

        elif transform_function == 'grayscale':
            transformed_pixels = grayscale(pixels)
            output_file = f"{file_name.split('.')[0]}_2x_trans_grayscale.png"

        elif transform_function == 'rotate_left':
            transformed_pixels = rotate_right(pixels)
            output_file = f"{file_name.split('.')[0]}_2x_trans_rotated.png"

        else:
            print("Función de transformación no válida.")
            return

    output_file = f"{file_name.split('.')[0]}_2x_transformed.png"
    write_img(pixels, output_file)
    print(f"Imagen transformada guardada como {output_file}")

def main():
    if len(sys.argv) < 3:
        print("Uso: python transform_multi.py <nombre_imagen> <funcion1> <args_funcion1> <funcion2> <args_funcion2> ...")
        return

    file_name = sys.argv[1]
    transformations = sys.argv[2:]

    apply_transforms(file_name, transformations)

if __name__ == '__main__':
    main()
