import sys
from images import read_img, write_img
from transforms import change_colors, rotate_colors, shift, crop, filter

def apply_transform(file_name, transform_function, *args):
    pixels = read_img(file_name)

    if transform_function == 'change_colors':
        if len(args) != 6:
            print("Esta función necesita 6 valores de RGB para poder cambiar la imagen.")
            return
        color_to_change = tuple(args[:3])
        new_color = tuple(args[3:])
        changed_pixels = change_colors(pixels, color_to_change, new_color)

        output_file = f"{file_name.split('.')[0]}_transformed.png"
        write_img(changed_pixels, output_file)
        print(f"Imagen transformada guardada como {output_file}")

    elif transform_function == 'rotate_colors':
        if len(args) != 1:
            print("Esta función necesita un valor de incremento.")
            return
        increment = int(args[0])

        changed_pixels = rotate_colors(pixels, increment)

        output_file = f"{file_name.split('.')[0]}_rotated_colors.png"
        write_img(changed_pixels, output_file)
        print(f"Imagen con colores rotados guardada como {output_file}")

    elif transform_function == 'shift':
        if len(args) != 2:
            print("Esta función necesita 2 números enteros (horizontal y vertical).")
            return

        horizontal_shift = int(args[0])
        vertical_shift = int(args[1])

        shifted_pixels = shift(pixels, horizontal_shift, vertical_shift)

        output_file = f"{file_name.split('.')[0]}_shifted.png"
        write_img(shifted_pixels, output_file)
        print(f"Imagen desplazada guardada como {output_file}")

    elif transform_function == 'crop':
        if len(args) != 4:
            print("Esta función necesita 4 números enteros (x, y, altura y ancho).")
            return

        x = int(args[0])
        y = int(args[1])
        width = int(args[2])
        height = int(args[3])

        cropped_pixels = crop(pixels, x, y, width, height)

        output_file = f"{file_name.split('.')[0]}_cropped.png"
        write_img(cropped_pixels, output_file)
        print(f"Imagen recortada guardada como {output_file}")

    elif transform_function == 'filtro':
        if len(args) != 3:
            print("Esta función requiere de tres valores para multiplicador de RGB")
            return

        filtered_pixels = filter(pixels, *args)

        output_file = f"{file_name.split('.')[0]}_filtered.png"
        write_img(filtered_pixels, output_file)
        print(f"Imagen con filtro guardada como {output_file}")

    else:
        print("Función de transformación no válida.")
        return

def main():
    if len(sys.argv) < 4:
        print("Uso: python transforms_args.py <nombre_imagen> <nombre_funcion_transformacion> <args_funcion>")
        return

    file_name = sys.argv[1]
    transform_function = sys.argv[2]
    args = list(map(int, sys.argv[3:]))

    apply_transform(file_name, transform_function, *args)

if __name__ == '__main__':
    main()
